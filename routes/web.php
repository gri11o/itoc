<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Http\Controllers\reports\DynamicReportController;
use App\Http\Controllers\reports\ReportController;

Auth::routes();

// --- habilidades, roles, usuarios
Route::get('users-list', 'Admin\UserController@getAll');
Route::get('roles-list', 'Admin\RoleController@getAll');
Route::get('abilities-list', 'Admin\AbilityController@getAll');
//------------------------------------
Route::get('documents-list', 'DocumentController@getAll');
Route::get('scripts-list', 'ScriptController@getAll');
Route::get('devices-list', 'DeviceController@getAll');
Route::post('devices-save', 'DeviceController@save');
Route::post('devices-connect', 'DeviceController@connect');
Route::get('devices-tree-list', 'DeviceTreeController@getTree');

// --- Se crea un prefijo para eliminar el /admin/ de todas las url ---//
Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::resource('/users', 'Admin\UserController');
    Route::resource('/roles', 'Admin\RoleController');
    Route::resource('/abilities', 'Admin\AbilityController');
});

// --- Se agrupan para aplicar el middleware de auth ---//
Route::middleware(['auth'])->group(function () {
    Route::get('tools', 'ToolController@index')->name('tools');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/user/profile', 'ProfileController@breadcrumb')->name('profile');
    Route::resource('documents', 'DocumentController');
    Route::resource('devices', 'DeviceController');
    Route::resource('scripts', 'ScriptController');
    /*
     * Trees
     */
    Route::resource('devices-tree', 'DeviceTreeController');
});

// --- Cambiar el lenguaje ---//
Route::get('lang/{locale}', 'LangController@index');
Route::post('document-image-upload', 'DocumentController@imageUpload')->name('image.upload');

// Modificacion de la imagen de perfil del usuario
Route::post('/modificar-img-perfil', 'ImageProfileController@test');

// Reports section
Route::get('/show_reports', [ReportController::class, 'homeReports']);
