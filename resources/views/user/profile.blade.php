@extends('layouts.master')

@section('content')
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="alert alert-success d-none" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Exitoso! </strong> Se ha modificado correctamente la imagen del perfil.
                </div>

                <div class="alert alert-danger d-none" id="failed-alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Sin exito! </strong> No fue posible modificar la imagen del perfil.
                </div>


                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> 
                                    <img src="{{ verify_image_profile() }}" class="img-circle" id="imgPerfilPrincipal" width="150" />

                                    <h4 class="card-title m-t-10">{{ Auth::user()->name }}</h4>
                                    <h6 class="card-subtitle">Desarrollador Full Stack Triara</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-12">
                                            {{-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCambioImagenPerfil">
                                                <i class="fas fa-camera-retro"></i>  Modificar Foto de Perfil
                                            </button> --}}
                                            <label class="btn btn-info">
                                                <i class="fas fa-camera-retro"></i>  Modificar Foto de Perfil
                                                <input type="file" id="inputFileImagenPerfil" accept="image/png, image/jpeg" hidden>
                                            </label>
                                        </div>                                     
                                    </div>
                                    <br>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                                    </div>

                                    {{-- modal para el cambio de imagen de perfil --}}
                                    <div class="modal fade" id="modalCambioImagenPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered w-75" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h3 class="modal-title" id="exampleModalLongTitle">Modificar Perfil</h3>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                
                                                {{-- modal croppie js --}}
                                                <div id="imagenPerfilCroppie"></div>
                                            </div>
                                            <br><br>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                              <button type="button" id="btn-cambiar-foto-perfil" class="btn btn-success">Cambiar</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>{{ Auth::user()->email }}</h6> <small class="text-muted p-t-30 db">Tel.:</small>
                                <h6>+51 8126 198930</h6> <small class="text-muted p-t-30 db">Dirección</small>
                                <h6>134 Antares Barrio estrella, Monterrey N.L. México</h6>
                                <div class="map-box">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div> <small class="text-muted p-t-30 db">Social</small>
                                <br/>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-facebook-f"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#activity" role="tab">Actividad</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#competitions" role="tab">Competencias</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#permissions" role="tab">Permisos</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Ajustes</a> </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- activity pane -->
                                <div class="tab-pane active" id="activity" role="tabpanel">
                                    <div class="card-body">
                                        <div class="profiletimeline">
                                            <div class="sl-item">
                                                <div class="sl-left"> <img src="{{ URL::asset('theme/assets/images/users/1.jpg') }}" alt="user" class="img-circle" /> </div>
                                                <div class="sl-right">
                                                    <div><a href="javascript:void(0)" class="link">Javier Medina</a> <span class="sl-date">Hace 5 minutos</span>
                                                        <p>Asigna tarea - <a href="javascript:void(0)"> Diseño de layout</a></p>
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-6 m-b-20"><img src="{{ URL::asset('theme/assets/images/big/img1.jpg') }}" class="img-responsive radius" /></div>
                                                            <div class="col-lg-3 col-md-6 m-b-20"><img src="{{ URL::asset('theme/assets/images/big/img2.jpg') }}" class="img-responsive radius" /></div>
                                                            <div class="col-lg-3 col-md-6 m-b-20"><img src="{{ URL::asset('theme/assets/images/big/img3.jpg') }}" class="img-responsive radius" /></div>
                                                            <div class="col-lg-3 col-md-6 m-b-20"><img src="{{ URL::asset('theme/assets/images/big/img4.jpg') }}" class="img-responsive radius" /></div>
                                                        </div>
                                                        <div class="like-comm"> <a href="javascript:void(0)" class="link m-r-10">2 comentarios</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 </a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"> <img src="{{ URL::asset('theme/assets/images/users/8.jpg') }}" alt="user" class="img-circle" /> </div>
                                                <div class="sl-right">
                                                    <div> <a href="javascript:void(0)" class="link">Rubi Mendez</a> <span class="sl-date">Hace 5 minutos</span>
                                                        <div class="m-t-20 row">
                                                            <div class="col-md-3 col-xs-12"><img src="{{ URL::asset('theme/assets/images/users/7.jpg') }}" alt="user" class="img-responsive radius" /></div>
                                                            <div class="col-md-9 col-xs-12">
                                                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. </p> <a href="javascript:void(0)" class="btn btn-success"> ¡Enviarme avance del plan!</a></div>
                                                        </div>
                                                        <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 </a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"> <img src="{{ URL::asset('theme/assets/images/users/5.jpg') }}" alt="user" class="img-circle" /> </div>
                                                <div class="sl-right">
                                                    <div><a href="javascript:void(0)" class="link">Miguel Pérez</a> <span class="sl-date">Hace 5 minutos</span>
                                                        <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                                    </div>
                                                    <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comentarios</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 </a> </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"> <img src="{{ URL::asset('theme/assets/images/users/5.jpg') }}" alt="user" class="img-circle" /> </div>
                                                <div class="sl-right">
                                                    <div><a href="javascript:void(0)" class="link">Miguel Pérez</a> <span class="sl-date">Hace 5 minutos</span>
                                                        <blockquote class="m-t-10">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                                        </blockquote>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- competitions pane -->
                                <div class="tab-pane" id="competitions" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre completo</strong>
                                                <br>
                                                <p class="text-muted">{{ Auth::user()->name }}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Móvil</strong>
                                                <br>
                                                <p class="text-muted">(044) 00 0000 0000</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">{{ Auth::user()->email }}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Localidad</strong>
                                                <br>
                                                <p class="text-muted">Monterrey</p>
                                            </div>
                                        </div>

                                        <h4 class="font-medium m-t-30">Set de habilidades</h4>
                                        <hr>
                                        <h5 class="m-t-30">PHP <span class="pull-right">80%</span></h5>
                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                                        </div>
                                        <h5 class="m-t-30">HTML 5 <span class="pull-right">90%</span></h5>
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                                        </div>
                                        <h5 class="m-t-30">jQuery <span class="pull-right">50%</span></h5>
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                                        </div>
                                        <h5 class="m-t-30">C# <span class="pull-right">70%</span></h5>
                                        <div class="progress">
                                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- settings pane -->
                                <div class="tab-pane" id="permissions" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre completo</strong>
                                                <br>
                                                <p class="text-muted">{{ Auth::user()->name }}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Móvil</strong>
                                                <br>
                                                <p class="text-muted">(044) 00 0000 0000</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">{{ Auth::user()->email }}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Localidad</strong>
                                                <br>
                                                <p class="text-muted">Monterrey</p>
                                            </div>
                                        </div>

                                        <h4 class="font-medium m-t-30">Set de permisos</h4>
                                        <hr>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Permiso</th>
                                                    <th>Tag</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach (Auth::user()->getAbilities() as $ability)
                                                    <tr>
                                                        <td>{{ $ability->title }}</td>
                                                        <td><span class="label label-danger">{{ $ability->name }}</span></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                        <div class="col">

                                        </div>
                                    </div>
                                </div>
                                <!-- settings pane -->
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Nombre completo</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="{{ Auth::user()->name }}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="{{ Auth::user()->email }}" class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="password" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Móvil</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="(044) 00 0000 0000" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Descripción</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" class="form-control form-control-line"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">País</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-line">
                                                        <option>Alemania</option>
                                                        <option>India</option>
                                                        <option>USA</option>
                                                        <option>México</option>
                                                        <option>Japón</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Actualizar Perfil</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- end Tab panes-->

                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

@endsection