<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'ITOC') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Automatización ITOC">
    <meta name="author" content="Jose Luis Hernandez">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('theme/assets/images/logo-icon.png') }}">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/node_modules/datatables/media/css/dataTables.bootstrap4.css') }}">

    <!-- Summer Note -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/node_modules/summernote/dist/summernote-bs4.css') }}">

    <!-- Toast -->
    <link rel="stylesheet" href="{{ URL::asset('theme/assets/node_modules/toast-master/css/jquery.toast.css') }}">

    <!-- Ribbons -->
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/pages/ribbon-page.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/style.min.css') }}">


    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/pages//select2.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/pages/multi-select.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/pages/bootstrap-select.min.css') }}">

    {{-- croppie js css --}}
    <link rel="stylesheet" href="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/profile-photo-croppie.css') }}" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="http://itoc-tools.triara.mexico/itoc/resources/libraries//jstree/themes/default/style.min.css" type="text/css" rel="stylesheet"/>

    <style type="text/css">
        div.dt-buttons {
            float: right;
        }
        blockquote.blockquote {
            color: #666;
            background: #f9f9f9;
            font-size: 1.1em;
            border-left: 5px solid #ccc;
            margin: 1.5em 15px;
            padding: 0.5em 15px;
            quotes: "\201C""\201D""\2018""\2019";
        }
        blockquote p {
            display: inline;
        }
        .panel-fullscreen {
            display: block;
            z-index: 9999;
            position: fixed !important;
            width: 100%;
            height: 100%;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            overflow: auto;
        }
    </style>
</head>