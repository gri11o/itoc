@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12">

            <div class="card">
                
                <div class="card-body bg-light border-bottom">
                    <div class="row p-0 m-0">
                        <div class="col-sm-9">
                            <h3>Administrador de dispositivos</h3>
                        </div>
                        <div class="col-sm-3">
                            <div class="text-right">
                                <div class="btn-group" role="group" aria-label="Opciones">
                                    <button type="button" class="btn btn-sm btn-circle btn-secondary">
                                        <i class="mdi mdi-download"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-circle btn-secondary">
                                        <i class="mdi mdi-share-variant"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-circle btn-secondary fullscreen-btn" id="panel-fullscreen">
                                        <i class="fas fa-expand"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row p-0 m-0">
                    <div class="col-xlg-1 col-lg-1 col-md-1 col-sm-2 p-r-0">
                        <div class="card-body p-1 text-center">
                            <div>
                                <button class="btn btn-circle btn-secondary" id="devices-tree-new-folder">
                                    <i class="far fa-folder"></i>
                                </button>
                            </div>
                            <div>
                                <button class="btn btn-circle btn-secondary" id="devices-tree-new-host">
                                    <i class="far fa-hdd"></i>
                                </button>
                            </div>
                            &nbsp;
                            <div>
                                <button class="btn btn-circle btn-primary" id="devices-tree-delete"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xlg-2 col-lg-3 col-md-4 col-sm-4">
                        <div class="card-body p-0 m-0">
                            <div id="devices-tree" style="height: 600px; overflow: auto;"></div>
                        </div>
                    </div>
                    <div class="col-xlg-9 col-lg-8 col-md-7 col-sm-6 p-0 m-0 border-left">
                        <!-- Tabs -->
                            <div class="card-body bg-white m-0 p-t-0">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#general-data" role="tab">General</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#advanced" role="tab">Avanzado</a>
                                    </li>
                                </ul>
                                <div class="tab-content">

                                    <!-- general data pane -->
                                    <div class="tab-pane active" id="general-data" role="tabpanel">
                                        <div class="card-body col">
                                            <form action="" class="">

                                                <input type="hidden" id="id" name="id" value="null">

                                                <fieldset id="device_disabled" disabled="disabled">

                                                    <div class="row">
                                                        <div class="col-12 p-b-20">
                                                            <div id="device_path"></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="host" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5">
                                                            Host:
                                                        </label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" id="host" name="host">

                                                            <span class="help-block text-danger">
                                                                <small id="host_error"></small>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!--
                                                    <div class="form-group row">
                                                        <label for="port" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5 text-right">Puerto:</label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" id="port" name="port">
                                                        </div>
                                                    </div>
                                                    -->
                                                    <div class="form-group row">
                                                        <label for="os" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5">
                                                            Sistema Operativo:
                                                        </label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <select class="form-control" id="os" name="os">
                                                                <option value="">&lt;Seleccione&gt;</option>
                                                                <option value="Linux">Linux</option>
                                                                <option value="Windows">Windows</option>
                                                            </select>

                                                            <span class="help-block text-danger">
                                                                <small id="os_error"></small>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="user" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5">
                                                            Usuario:
                                                        </label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <input type="text" class="form-control" id="user" name="user">

                                                            <span class="help-block text-danger">
                                                                <small id="user_error"></small>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="pass" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5">
                                                            Password:
                                                        </label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <input type="password" class="form-control" id="password" name="password">

                                                            <span class="help-block text-danger">
                                                                <small id="password_error"></small>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="description" class="control-label col-xlg-2 col-lg-3 col-md-5 col-sm-5">
                                                            Descripción:
                                                        </label>
                                                        <div class="col-xlg-10 col-lg-9 col-md-7 col-sm-7">
                                                            <textarea class="form-control" id="description" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form> 
                                        </div>
                                    </div>

                                    <!-- advanced data pane -->
                                    <div class="tab-pane" id="advanced" role="tabpanel">
                                        <div class="card-body p-0 m-0">
                                            <div id="advanced-body">
                                                <div id="advanced-content" style="width: 100%; height: 330px; background-image:url({{ URL::asset('theme/assets/images/big/bg.png') }}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end Tab panes-->

                            </div>
                        <!-- end Tabs -->
                    </div>
                </div>
                <div class="card-footer text-right border-top">
                    <div class="btn-group" role="group" aria-label="Opciones">
                        <button class="btn btn-secondary btn-lg btn-circle" id="connect"><i class="fas fa-plug fa-rotate-90"></i></button>
                        <button class="btn btn-secondary btn-lg btn-circle" id="save"><i class="far fa-save"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection