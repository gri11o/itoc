@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <h4 class="card-title">EDITAR EL ROL {{ $role->id }} </h4>
                <h6 class="card-subtitle"> Editar rol.</h6>
                <form class="form-material m-t-40" method="POST" action="{{ route('roles.update', $role->id) }}">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Nombre</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="Nombre" id="name" name="name" value="{{ $role->name }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Descripción</label>
                        <div class="col-10">
                            <input type="text" class="form-control form-control-line" placeholder="Descripción" id="title" name="title" value="{{ $role->title }}" required>
                        </div>
                    </div>
                    <div class="form-group m-t-40 row">
                        <label class="col-2 col-form-label text-right">Habilidades</label>
                        <div class="col-10">
                            <select class="select2 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="abilities[]" name="abilities[]" required>
                                @foreach($abilities as $abilitie)
                                <option old('abilities') @foreach($role->getAbilities()->pluck('id', 'id') as $roleAbilitie)
                                    @if($abilitie->id == $roleAbilitie)
                                    selected = "selected"
                                    @endif
                                    @endforeach>{{$abilitie->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row text-right">
                        <div class="col">
                            <button type="submit" class="btn btn-success" id="save">
                                Actualizar
                            </button>
                            <a href="{{ route('roles.index') }}" class="btn btn-warning">Cancelar </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection