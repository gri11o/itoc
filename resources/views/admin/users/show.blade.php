@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 text-right">
                        @if ((Auth::user()->can('users_manage')))
                        <a href="{{ route('users.edit', $user->id) }}">
                            <button type="button" class="btn btn-circle btn-primary" onclick="javascript();">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                        </a>
                        @endif
                    </div>
                </div>
                <h4 class="card-title">Detalle del usuario: {{ $user->id }} </h4>
                <h6 class="card-subtitle"> Detalle del usuario.</h6>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Nombre</label>
                    <label class="col-10 col-form-label">{{ $user->name }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Email</label>
                    <label class="col-10 col-form-label">{{ $user->email }}</label>
                </div>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Roles</label>
                    <div class="col-10">
                        @foreach ($user->roles()->pluck('name') as $role)
                            <span class="label label-info label-many">{{ $role }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Permisos</label>
                    <div class="col-10">
                        @foreach ($user->abilities->pluck('name') as $ability)
                            <span class="label label-info label-many">{{ $ability }}</span>
                        @endforeach
                    </div>
                </div>
                <br>
                <div class="row text-right">
                    <div class="col">
                        <a href="{{ route('users.index') }}" class="btn btn-warning">Cancelar </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection