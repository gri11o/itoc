@extends('layouts.app')

@section('content')
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar"
    style="background-image: url( '{{ asset('theme/assets/images/background/login-register.jpg') }}' )">
    <div class="login-box card">
        <div class="card-body">
            <form method="POST" class="form-horizontal form-material text-center" id="loginform"
                action="{{ route('login') }}">
                @csrf
                <a href="javascript:void(0)" class="db"><img src="{{ asset('theme/assets/images/logo-icon.png') }}"
                        alt="Home" width="60px" /><br /><img src="{{ asset('theme/assets/images/logo-text.png') }}" alt="Home" width="70px" /></a>
                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input id="email" type="email" placeholder=" {{ __('E-Mail Address') }} "
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                            value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" placeholder=" {{ __('Password') }} "
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                            required>

                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="d-flex no-block align-items-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="remember" name="remember"
                                    {{ old( 'remember' ) ? 'checked' : '' }} >
                                <label class="custom-control-label" for="remember"> {{ __('Remember Me') }} </label>
                            </div>
                            <!-- <div class="ml-auto">
                                <a href="javascript:void(0)" id="to-recover" class="text-muted"><i
                                        class="fas fa-lock m-r-5"></i> Forgot pwd?</a>
                            </div> -->
                        </div>
                    </div>
                </div>

                <!-- <div class="col-md-12">
                    @php $locale = session()->get('locale'); @endphp
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="flag-icon {{ 'flag-icon-'.$locale }}"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                        <a class="dropdown-item" href="lang/es"><i class="flag-icon flag-icon-es"></i> Spanish</a>
                        <a class="dropdown-item" href="lang/us"><i class="flag-icon flag-icon-us"></i> English</a>
                    </div>
                </div> -->

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>

                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        {{ __('Dont have an account?') }} <a href="{{ route('register') }}" class="text-primary m-l-5">
                            <b> {{ __('Sign Up') }} </b>
                        </a>
                    </div>
                </div>
            </form>
            <!-- <form class="form-horizontal" id="recoverform" action="index.html">
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Email">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
                            type="submit">Reset</button>
                    </div>
                </div>
            </form> -->
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
@endsection