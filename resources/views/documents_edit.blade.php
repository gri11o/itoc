@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <!--<h4 class="card-title text-muted">Nuevo</h4>-->

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-material" method="POST" action="{{ route('documents.update',$document->id) }}">

                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="row">

                            <div class="col-12">
                                <input 
                                    style="font-size: 34px;" 
                                    type="text"
                                    class="form-control"
                                    id="title" 
                                    name="title"
                                    value="{{ old('title', $document->title) }}" 
                                    placeholder="Escribe aquí el título"
                                >
                            </div>
                            <div class="col-12">
                                <textarea style="font-size: 14px;" rows="3" class="form-control" name="description" id="description" placeholder="Aquí la descripción">{{ old('description', $document->description) }}</textarea>
                            </div>
                            <!--
                            <div class="col-4">
                                <input type="text" class="form-control" id="tags" name="tags" value="">
                            </div>
                            -->
                        </div>

                        <div class="w-100">&nbsp;</div>

                        <div class="row">
                            <div class="col">
                                <textarea id="content" name="content" class="summernote">{!! old('content', $document->content) !!}</textarea>
                            </div>  
                        </div>

                        <div class="row text-right">
                            <div class="col">
                                <button type="submit" class="btn btn-success" id="save">
                                    <i class=""></i> Guardar
                                </button>

                            </div>                                    
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>

@endsection