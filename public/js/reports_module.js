$(function(){

    var format_selected = "";

    $(".dropdown-menu a").click(function(e){
        e.preventDefault();
        const selectedOptionText = $(this).text().trim();
        $("#dropdownMenuButton").text("Formato a descargar: " + selectedOptionText);

        if(selectedOptionText == "PDF"){
            format_selected = url_report_pdf;
        }else if(selectedOptionText == "HTML"){
            format_selected = url_report_html;
        }else if(selectedOptionText == "XLS"){
            format_selected = url_report_xls;
        }
    });

    $("#btnDownloadReport").click(function(e){
        if(format_selected){
            window.location.replace(format_selected);
        }
    });

});
