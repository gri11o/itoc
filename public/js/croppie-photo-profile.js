$(function () {
    rawImg = null;

    var imagenPerfilUsuario = $("#imagenPerfilCroppie").croppie({
        url: "http://itoc-tools/theme/assets/images/users/admin.png",
        viewport: {
            width: 400,
            height: 400,
            type: 'circle'
        }
    });

    function modificarImagenAjax(imagenCroppie) {
        //establece opacidad
        $('#imgPerfilPrincipal').css('opacity', '0.1');
        $('#imgProfileLeftSidebar').css('opacity', '0.1');

        let token = $('meta[name="csrf-token"]').attr('content');
        var formData = new FormData();
        formData.append('image', imagenCroppie);
        $.ajax({
            url: '/modificar-img-perfil',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            headers: { 'X-CSRF-TOKEN': token }
        }).done(function (resp) {
            $('#imgPerfilPrincipal').attr('src', resp.url_imagen_actualizada);
            $('#imgProfileLeftSidebar').attr('src', resp.url_imagen_actualizada);
  
            $('#success-alert').removeClass("d-none");
            $("#success-alert").fadeTo(2000, 1000).slideUp(500, function () {
                $("#success-alert").slideUp(500);
            });
        }).fail(function (resp) {
            $('#failed-alert').removeClass("d-none");
            $("#failed-alert").fadeTo(2000, 1000).slideUp(500, function () {
                $("#failed-alert").slideUp(500);
            });
        });

        $('#imgPerfilPrincipal').fadeTo("slow", 1);
        $('#imgProfileLeftSidebar').fadeTo("slow", 1);
    }

    $('#modalCambioImagenPerfil').on('shown.bs.modal', function () {
        imagenPerfilUsuario.croppie('bind', { url: rawImg });
    })

    $('#modalCambioImagenPerfil').on('hide.bs.modal', function () {
        $('#inputFileImagenPerfil').val('');
    })

    $('#btn-cambiar-foto-perfil').on('click', function (e) {
        imagenPerfilUsuario.croppie(
            "result", {
            type: "base64",
            format: "png"
        }).then(function (resp) {
            $('#imgPerfilPrincipal').attr('src', resp);
            $('#modalCambioImagenPerfil').modal('hide');
            modificarImagenAjax(resp);
        });
    });

    $('#inputFileImagenPerfil').change(function () {
        idobtenido = $(this).data('id');
        tempFile = $(this).val();

        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                rawImg = e.target.result;
            }
            reader.readAsDataURL(this.files[0]);
            $('#modalCambioImagenPerfil').modal('show');
        }

    });
});
