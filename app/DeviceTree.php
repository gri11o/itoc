<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class DeviceTree extends Model
{
	protected $guarded = [];
	use NodeTrait;

    /**
     * Obtiene el árbol con los dispositivos de un usuario.
     */
    public function tree()
    {
        return $this->morphOne('App\Tree', 'treeable');
    }

    /**
     * Obtiene un árbol de acuerdo a su identificador.
     */
    public static function getById($id)
    {
		$tree = [];
        $data = self::descendantsAndSelf($id)->toArray();

        foreach ($data as $key => $value)
        {
            if ( $value['parent_id'] === null )

            {
                $value['parent_id'] = '#';
            }
            array_push(
                $tree,
                [
                    'id' => $value['id'],
                    'parent' => $value['parent_id'],
                    'text' => $value['name'],
                    'type' => $value['type']
                ]
            );
        }

        return $tree;
    }
}
