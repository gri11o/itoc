<?php

namespace App\Http\Controllers;

use App\Device;
use App\Libraries\Cipher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $devices = Device::all();

        return datatables()->of($devices)->toJSON();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('devices')->with([
            'title' => 'Dispositivos',
            'breadcrumb' => [
                [
                    'title' => 'Dispositivos'
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            'title' => 'Dispositivos',
            'breadcrumb' => [
                [
                    'title' => 'Dispositivos',
                    'url' => URL::route('devices.index')
                ],
                [
                    'title' => 'Administrador'
                ]
            ]
        ];
        
        return view('devices_create')
            ->with($breadcrumb);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(Device $device)
    {
        //
    }

    /**
     * Process ajax request.
     * Obtiene un string de conexión a un dispositivo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function connect(Request $request)
    {
        $request->validate(
            [
                'host' => 'required',
                'os' => 'required',
                'user' => 'required',
                'password' => 'required'
            ]
        );

        if ($request->os == 'Linux') {

            $params = '{
                "identifier": "ITOC Triara",
                "username" : "' . Auth::user()->name . '",
                "expires" : "1656608423000",
                "connections" : {
                    "Connection SSH ' . $request->host . '" : {
                        "protocol" : "ssh",
                        "parameters" : {
                            "port" : "22",
                            "hostname": "' . $request->host . '",
                            "username": "' . $request->user . '",
                            "password": "' . $request->password . '",
                            "enable-sftp": "true"
                        }
                    }
                }
            }';

        } else{

            $params = '{
                "identifier": "ITOC Triara",
                "username" :"' . Auth::user()->name . '",
                "expires" : "1656608423000",
                "connections" : {
                    "Connection RDP ' . $request->host . '" : {
                        "protocol" : "rdp",
                        "parameters" : {
                            "port" : "3389",
                            "hostname": "' . $request->host . '",
                            "username": "' . $request->user . '",
                            "password": "' . $request->password . '",
                            "security": "nla",
                            "ignore-cert": "true",
                            "enable-wallpaper": "true",
                            "enable-drive": "true"
                        }
                    }
                }
            }';

        }

        $secret = '26kozQaKwRuNJ24t';

        $encrypted = Cipher::encrypt($params, $secret);

        return "http://172.20.45.151:8080/guacamole/#/api/tokens?data=".str_replace("+", "_", $encrypted);
    }
}