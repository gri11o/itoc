<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\DeviceTree;
use App\Tree;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';

    protected function registered()
    {
        return redirect()->route('profile');
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        // --- Asignar rol guest
        $user->assign('Guest');
        // --- Asignar arbol
        $this->setDeviceTree($user);
        return $user;
    }

    // --- Se asigna un arbol para los dispositibos 
    protected function setDeviceTree(User $user)
    {
        $nodes = [
            'name' => 'Dispositivos',
            'type' => 'branch'
        ];

        $tree = DeviceTree::create($nodes);

        // $tree->id (id del árbol creado)
        Tree::create([
            'user_id' => $user->id,
            'name' => 'Device Tree',
            'treeable_id' => $tree->id,
            'treeable_type' => DeviceTree::class
        ]);
    }
}
