<?php

namespace App\Http\Controllers;

use Validator;
use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class DocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $documents = Document::with('user')->get();

        return datatables()->of($documents)->toJson();
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function imageUpload(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        $fullPath  = public_path('images');
        request()->image->move($fullPath, $imageName);

        return asset("/images/$imageName");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('documents')->with([
            'title' => 'Documentos',
            'breadcrumb' => [
                [
                    'title' => 'Documentos'
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documents_create')->with([
            'title' => 'Documentos',
            'breadcrumb' => [
                [
                    'title' => 'Documentos',
                    'url' => URL::route('documents.index')
                ],
                [
                    'title' => 'Nuevo'
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title' => 'required|max:64|unique:documents',
                'description' => 'max:256',
                'content' => 'required'
            ],
            [
                'title.required' => 'El título es necesario, escoge sabiamente',
                'title.max' => 'El título no debe ser mayor a 50 caracteres, no exageres',
                'title.unique' => 'El título ya está en uso, se original por favor',
                'description.max' => 'La descripción no debe ser mayor a 155 caracteres, no exageres',
                'content.required'=> 'El contenido es necesario, no bromees'
            ]
        );
        

        $doc = new Document;
        $doc->user_id = Auth::user()->id;
        $doc->title = $request->input('title');
        $doc->description = $request->input('description');
        $doc->content = $request->input('content');
        $doc->save();

        $request->session()->flash(
            'notification', [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El documento se guardo correctamente',
            ]
        );

        return view('documents')->with([
            'title' => 'Documentos',
            'breadcrumb' => [
                [
                    'title' => 'Documentos'
                ]
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        $document->load('user');

        //dd($document);

        return view('documents_show')->with([
            'title' => 'Documentos',
            'breadcrumb' => [
                [
                    'title' => 'Documentos',
                    'url' => URL::route('documents.index')
                ],
                [
                    'title' => mb_strimwidth($document->title, 0, 30, "...") 
                ]
            ],
            'document' => $document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //dd($document);
        
        return view('documents_edit')->with([
            'title' => 'Documentos',
            'breadcrumb' => [
                [
                    'title' => 'Documentos',
                    'url' => URL::route('documents.index')
                ],
                [
                    'title' => mb_strimwidth($document->title, 0, 30, "...") 
                ]
            ],
            'document' => $document
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $request->validate(
            [
                'title' => 'required|max:64',
                'description' => 'max:256',
                'content' => 'required'
            ],
            [
                'title.required' => 'El título es necesario, escoge sabiamente',
                'title.max' => 'El título no debe ser mayor a 50 caracteres, no exageres',
                'title.unique' => 'El título ya está en uso, se original por favor',
                'description.max' => 'La descripción no debe ser mayor a 155 caracteres, no exageres',
                'content.required'=> 'El contenido es necesario, no bromees'
            ]
        );

        $document->update($request->all());

        $request->session()->flash(
            'notification', [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El documento se actualizó correctamente',
            ]
        );

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }
}
