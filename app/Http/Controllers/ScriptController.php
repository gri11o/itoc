<?php

namespace App\Http\Controllers;

use App\Script;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;

class ScriptController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $scripts = Script::all();
        return datatables()->of($scripts)->toJson();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('scripts')->with([
            'title' => 'Scripts',
            'breadcrumb' => [
                [
                    'title' => 'Scripts',
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $templates = Template::all();
        $title = 'Scripts';
        $breadcrumb = [
            [
                'title' => 'Scripts',
                'url' => URL::route('scripts.index'),
            ],
            [
                'title' => 'Nuevo',
            ],
        ];

        return view('scripts_create')->with(compact('templates', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validar datos
        $messages = [
            'name.required' => 'Es necesario ingresar un nombre para identificar el script.',
            'name.min' => 'El nombre del script debe contener al menos 3 caracteres.',
            'description.required' => 'Es necesario ingresar una descripcion para el script.',
            'description.max' => 'La descripcion debe contener no mas de 500 caracteres.',
            'url.required' => 'Es necesario ingresar una url para el script.',
            'version.required' => 'Es necesario ingresar una varsion para el script.',
            'version.numeric' => 'El valor de la version debe ser numerico.',
            'language.required' => 'Es necesario ingresar un lenguaje para el script.',
            'language.min' => 'El valor para lenguaje debe contener por lo menos 3 caracteres.',
            'language.max' => 'El valor para lenguaje debe contener no mas de 20 caracteres.',
        ];
        $rules = [
            'name' => 'required | min:3',
            'description' => 'required | max:500',
            'url' => 'required',
            'version' => 'required | numeric',
            'language' => 'required | min:3 | max:20',
        ];

        $this->validate($request, $rules, $messages);

        $script = new Script();
        // --- Se pasa a json el valor enviado desde el form del objeto select ---
        // --- para obtener el id del template -----------------------------------
        $template = json_decode($request->input('template_id'), true);
        // -----------------------------------------------------------------------
        $script->template_id = $template['id'];
        $script->name = $request->input('name');
        $script->type = $request->input('type');
        $script->api = $request->input('api');
        $script->description = $request->input('description');
        $script->url = $request->input('url');
        $script->version = $request->input('version');
        $script->language = $request->input('language');
        $script->author = Auth::user()->id;
        $script->save();

        $request->session()->flash(
            'notification', [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El script se guardo correctamente',
            ]
        );

        return redirect('scripts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Script  $script
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $script = Script::find($id);
        $template = Template::find($script->template_id);

        $title = 'Detalle';
        $breadcrumb = [
            [
                'title' => 'Detalle',
                'url' => URL::route('scripts.index'),
            ],
            [
                'title' => 'Detalle',
            ],
        ];

        return view('scripts_show')->with(compact('script', 'template', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $script = Script::find($id);
        $templates = Template::all();

        $title = 'Scripts';
        $breadcrumb = [
            [
                'title' => 'Scripts',
                'url' => URL::route('scripts.index'),
            ],
            [
                'title' => 'Editar',
            ],
        ];

        return view('scripts_edit')->with(compact('script', 'templates', 'title', 'breadcrumb'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Script  $script
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validar datos
        $messages = [
            'name.required' => 'Es necesario ingresar un nombre para identificar el script.',
            'name.min' => 'El nombre del script debe contener al menos 3 caracteres.',
            'description.required' => 'Es necesario ingresar una descripcion para el script.',
            'description.max' => 'La descripcion debe contener no mas de 500 caracteres.',
            'url.required' => 'Es necesario ingresar una url para el script.',
            'version.required' => 'Es necesario ingresar una varsion para el script.',
            'version.numeric' => 'El valor de la version debe ser numerico.',
            'language.required' => 'Es necesario ingresar un lenguaje para el script.',
            'language.min' => 'El valor para lenguaje debe contener por lo menos 3 caracteres.',
            'language.max' => 'El valor para lenguaje debe contener no mas de 20 caracteres.',
        ];
        $rules = [
            'name' => 'required | min:3',
            'description' => 'required | max:500',
            'url' => 'required',
            'version' => 'required | numeric',
            'language' => 'required | min:3 | max:20',
        ];

        $this->validate($request, $rules, $messages);

        $script = Script::find($id);
        // --- Se pasa a json el valor enviado desde el form del objeto select ---
        // --- para obtener el id del template -----------------------------------
        $template = json_decode($request->input('template_id'), true);
        // -----------------------------------------------------------------------
        $script->template_id = $template['id'];
        $script->name = $request->input('name');
        $script->type = $request->input('type');
        $script->api = $request->input('api');
        $script->description = $request->input('description');
        $script->url = $request->input('url');
        $script->version = $request->input('version');
        $script->language = $request->input('language');
        $script->author = Auth::user()->id;
        $script->update();

        $request->session()->flash(
            'notification', [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El script se actualizó correctamente',
            ]
        );

        return redirect('scripts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Script  $script
     * @return \Illuminate\Http\Response
     */
    public function destroy(Script $script)
    {
        //
    }

}
