<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Script extends Model
{
    // Una script tiene un template.
    public function script_details()
    {
        return $this->belongsTo(Template::class);
    }
}
