<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\DeviceTree as Tree;

class DeviceTreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$nodes = [
			'name' => 'Dispositivos',
			'type' => 'branch'
		];

		Tree::create($nodes);
    }
}
