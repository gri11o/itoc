<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\DeviceTree;
use App\Tree;

class TreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Eloquent ORM
        $user = Tree::create([
            'user_id' => 1,
            'name' => 'Device Tree',
            'treeable_id' => 1,
            'treeable_type' => DeviceTree::class
        ]);
    }
}
