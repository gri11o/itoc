<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Eloquent ORM
        $user = User::create([
            'name' => 'Administrator',
            'email' => 'admin@triara.com',
            'password' => bcrypt('triara')
        ]);
        $user->assign('Administrator');

        factory(User::class,5)->create();
    }
}
